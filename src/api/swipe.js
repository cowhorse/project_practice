import request from "@/utils/request";

//请求轮播图
export const getSwipeApi = () =>
  request({
    url: "/swiper",
    method: "get",
  });

//请求活动图
export const getMoveaboutApi = () =>
  request({
    url: "/activitys",
    method: "get",
  });
// 请求秒杀列表
export const getSecondsApi = () =>
  request({
    url: "/seconds",
    method: "get",
  });

//请求商品列表
export const getCommodityApi = () =>
  request({
    url: "/goods",
    method: "get",
  });
//请求商品详情
export const getGoodsDetailsapi = (goodsId) =>
  request({
    url: "/goodsDetail",
    method: "get",
    params: { goodsId },
  });
