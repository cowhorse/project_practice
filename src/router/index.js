import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "home",
    component: () => import("@/views/layout/layoutView"),
    children: [
      {
        path: "/home",
        component: () => import("@/views/home/homeView"),
      },
      {
        path: "/my",
        component: () => import("@/views/my/myView"),
      },
      {
        path: "ShopCar",
        component: () => import("@/views/ShopCar/ShopCarView"),
      },
    ],
  },
  {
    path: "/productList",
    component: () => import("@/views/goodslist/productList"),
  },
  {
    path: "/Listdetails",
    component: () => import("@/views/goodsdetails/Listdetails"),
  },
  {
    path: "/buynow",
    component: () => import("@/views/buy/buynow"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
