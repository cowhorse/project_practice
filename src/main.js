import Vue from "vue";
import App from "./App.vue";
import router from "./router";
// 引入存入缓存封装工具包
import store from "./store";
//把手机屏幕划分为十等份作为html元素字体大小
import "amfe-flexible";
import Vant from "vant";
import "vant/lib/index.css";
Vue.use(Vant);
import "./style/index.less";
Vue.config.productionTip = false;
new Vue({
  // 写进去
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
