/**
 * 封装 axios 请求模块
 */
import axios from "axios";
// import store from "@/store";
// import router from "@/router";

const request = axios.create({
  baseURL: "http://api.imooc.hybrid.lgdsunday.club", // 全局配置根路径
});

export default request;
