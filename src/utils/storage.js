// import store from "../store";
// 封装存缓存的方法
// key:存缓存的键
// value：存缓存的值
export const setItem = (key, value) => {
  //思考：存一个简单数据没问题，那要存一个数组或对象，可以吗？
  // localStorage.setItem(key, JSON.stringify(value));
  if (typeof value == "object") {
    localStorage.setItem(key, JSON.stringify(value));
  } else {
    localStorage.setItem(key, value);
  }
};

// 封装取缓存的方法
export const getItem = (key) => {
  // 怎么判断读到的是普通字符串还是JSON字符串？
  try {
    return JSON.parse(localStorage.getItem(key));
  } catch (error) {
    return localStorage.getItem(key);
  }
};
