import Vue from "vue";
import Vuex from "vuex";
// 浏览器缓存的数据不具备响应性，Vuex的值不能够长期保存
// 优点：缓存：长期保存数据；Vuex：数据具备响应性
// 集二者之优点来保存用户token
import { setItem, getItem } from "@/utils/storage";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cart: getItem("cart") || [], //购物车
  },
  getters: {},
  mutations: {
    addToCart(state, val) {
      //如果数组长度为0直接往里放
      if (state.cart.length === 0) {
        Object.assign(val, { numb: 0 });
        Object.assign(val, { check: false });
        state.cart.push(val);
      }
      //如果数组长度不为0
      if (state.cart.length != 0) {
        // 原数组有则numb加一
        if (state.cart.find((item) => item.id == val.id)) {
          state.cart[state.cart.findIndex((item) => item.id == val.id)].numb++;
        }

        // 原数组没有就直接放一个带计数器的数组
        else {
          Object.assign(val, { numb: 1 });
          Object.assign(val, { check: false });
          state.cart.push(val);
        }
      }
      setItem("cart", state.cart);

      //存刷新token
    },
  },
  actions: {},
});
