module.exports = {
  plugins: {
    "postcss-pxtorem": {
      // 如果设计师给的设计图是2倍图，基准值75
      // 如果设计师给的设计图是1倍图，基准值37.5
      // 通俗的理解：设计师给的设计图的宽度除以10，就是我们的基准值
      // 结果：设置好基准值，页面尺寸直接照抄设计图即可
      rootValue({ file }) {
        //如果是vant的样式，基准值为37.5
        //非vant的样式，基准值为75（注意：根据设计图尺寸）
        return file.indexOf("vant") !== -1 ? 37.5 : 75;
      },
      propList: ["*"],
    },
  },
};
